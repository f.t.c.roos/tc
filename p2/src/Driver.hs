module Driver where

import Algebra
import Model
import Interpreter
import Lexer
import Parser
import ParseLib.Abstract.Core (parse)
-- Exercise 11
interactive :: Environment -> ArrowState -> IO ()
interactive env state = case step env state of 
    (Fail e)                             -> putStrLn (error e)
    (Done s p h)                         -> putStrLn (printSpace s ++ "Done!" )
    (Ok nextState@(ArrowState s p h cs)) -> do putStrLn (printSpace s)
                                               putStrLn "Print next ArrowState? (y/n)"                                               
                                               userInput <- getLine
                                               case userInput of
                                                   "y" -> interactive env nextState  -- advance to the next state
                                                   _   -> interactive env state      -- show the current state again                                           


batch :: Environment -> ArrowState -> (Space, Pos, Heading)
batch env state = case step env state of
    (Done s p h)   -> (s, p, h)
    (Ok nextState) -> batch env nextState
    (Fail e)       -> error e


main :: IO()
main = do putStr "Please enter path to Space file."
          spacePath <- getLine                          -- "C:/Users/6590136/Desktop/tc/p2/examples/AddInput.space"
          spaceFile <- readFile spacePath          
          putStr "Please enter path to Arrow file."
          arrowPath <- getLine                          -- "C:/Users/6590136/Desktop/tc/p2/examples/Add.arrow"
          arrowFile <- readFile spacePath
          putStr "Please enter x-coordinate of start position."
          xCoor     <- getLine 
          putStr "Please enter y-coordinate of start position."
          yCoor     <- getLine 
          putStr "Please enter Heading of start position (0=Left, 1=Up, 2=Right, 3=Down)"
          heading   <- getLine 
          putStr "Please enter the desired mode (i=interactive, b=batch)"
          modeInput <- getLine 
          let env       = toEnvironment arrowFile :: Environment
          let program   = calc.alexScanTokens $ arrowFile :: [Rule]
          let cs        = rulecmds $ head program
          let pos       = (read xCoor, read yCoor) :: Pos
          let h         = toEnum $ read heading :: Heading
          let space     = fst $ head $ parse parseSpace spaceFile          
          let arrowState = ArrowState space pos h cs

          case modeInput of
              "i" -> interactive env arrowState
              _   -> print $ batch env arrowState