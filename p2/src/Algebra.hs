module Algebra where

import Model
import Data.List

-- Exercise 5
type Algebra p r c a = ([r] -> p               -- program
                    , Identifier -> [c] -> r  -- rule
                    , c, c, c, c              -- go take mark nothing
                    , Direction -> c          -- turn
                    , Direction -> [a] -> c   -- case
                    , Identifier -> c         -- ident
                    , Pattern -> [c] -> a     -- alt
                    )

fold :: Algebra p r c a -> Program -> p
fold (fprogram, frule, fgo, ftake, fmark, fnothing, fturn, fcase, fident, falt) = foldProgram
  where foldProgram rules = fprogram $ map foldRule rules 
        foldRule (Rule ident commands) = frule ident $ map foldCommand commands
        foldCommand CGo = fgo
        foldCommand CTake = ftake
        foldCommand CMark = fmark
        foldCommand CNothing = fnothing
        foldCommand (CTurn d) = fturn d
        foldCommand (CCase d alts) = fcase d $ map foldAlt alts
        foldCommand (CIdentifier s) = fident s
        foldAlt (Alternative pattern commands) = falt pattern $ map foldCommand commands

-- Exercise 6
-- 1. No calls to undefined rules
-- 2. Rule named start
-- 3. No rule is defined twice
-- 4. No pattern match failure
checkProgram :: Program -> Bool
checkProgram program = checkNoUndefinedCalls
                    && checkContainsStart 
                    && checkNoDuplicateRules
                    && checkNoPatternMatchFailure
  where checkNoUndefinedCalls = noUndefinedCalls program
        checkContainsStart = containsStart program
        checkNoDuplicateRules = uniqueList $ map ruleident program
        checkNoPatternMatchFailure = validPatterns $ extractPatterns program

-- Checks if there are indeed no calls to undefined rules (1)
noUndefinedCalls :: Program -> Bool
noUndefinedCalls program = allIncluded calledRules definedRules
  where calledRules = fold calledRulesAlgebra program  -- list of identifiers of called rules
        definedRules = map ruleident program

-- Returns true if all of the elements in the first list are contained in the second
allIncluded :: [String] -> [String] -> Bool
allIncluded [] _ = True
allIncluded (x:xs) ys
  | x `notElem` ys = False 
  | otherwise = allIncluded xs ys

-- Checks if list of rules contain the element start (2)
containsStart :: Program -> Bool
containsStart program = "start" `elem` map ruleident program

-- Checks if all elements in a list are unique (3)
uniqueList :: Eq a => [a] -> Bool
uniqueList [_] = True
uniqueList (x:xs)
  | x `elem` xs = False
  | otherwise = uniqueList xs

-- Checks if all case expressions contain at least the five standard values OR the '_' value (4)
validPatterns :: [Pattern] -> Bool
validPatterns ps = (PEmpty `elem` ps
                && PLambda `elem` ps
                && PDebris `elem` ps
                && PAsteroid `elem` ps
                && PBoundary `elem` ps)
                || PAll `elem` ps

-- Extracts all direct patterns from a program
extractPatterns :: Program -> [Pattern]
extractPatterns rules = map altpattern alternatives
  where commands = concatMap rulecmds rules
        alternatives = concatMap getAlternatives commands
        getAlternatives (CCase _ a) = a
        getAlternatives _ = []

--                            program  rule     command  alternative
calledRulesAlgebra :: Algebra [String] [String] [String] [String]
calledRulesAlgebra = (concat,                   -- program
                      \_ commands -> concat commands,           -- rule
                      [],[],[],[],                              -- command: go take mark nothing
                      const [],                                 -- command: turn
                      \dir alternatives -> concat alternatives, -- command: case
                      const [],                                 -- command: alt
                      \pattern commands -> concat commands)     -- alternative
    