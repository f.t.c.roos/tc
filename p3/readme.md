
Submission by Floris Roos 6240992 and Danny Eek 6591356

# INFOB3TC – Assignment P3 - C#
A framework for the C# assignment of the course Talen & Compilers at the University of Utrecht.

## TO DO
1(0.5 pt). DONE
2(1 pt). DONE
3(0.5 pt). DONE
4(0.5 pt). DONE
5(1.0  pt). DONE
6(1.5  pt). ALMOST DONE, ADDED ENV BUT DID NOT FILL IT YET
7(1.0 pt). DONE
8(0.5 pt) DONE
9(1.5 pt). NOT DONE
10(2 pt). NOT DONE
11(bonus, 1 pt). NOT DONE
12(bonus, up to 1 pt). DONE

Total points: 10 + bonus of 2