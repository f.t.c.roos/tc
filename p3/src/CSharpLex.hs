module CSharpLex where

import Data.Char
import Control.Monad hiding ((<$))
import ParseLib.Abstract
import Prelude hiding ((<$), (<*), (*>))

data Token = POpen    | PClose      -- parentheses     ()
           | SOpen    | SClose      -- square brackets []
           | COpen    | CClose      -- curly braces    {}
           | Comma    | Semicolon
           | KeyIf    | KeyElse
           | KeyWhile 
           | KeyFor   
           | KeyPrint 
           | KeyReturn
           | KeyTry   | KeyCatch
           | KeyClass | KeyVoid           
           | StdType   String       -- the 8 standard types
           | Operator  String       -- the 15 operators
           | UpperId   String       -- uppercase identifiers
           | LowerId   String       -- lowercase identifiers
           | ConstInt  Int
           | ConstBool Bool
           | ConstChar Char
           deriving (Eq, Show)

----- Begin Lexer -----
lexicalScanner :: Parser Char [Token]
lexicalScanner = lexNonsense *> greedy (lexToken <* lexNonsense) <* eof
  where lexNonsense = lexWhiteSpace <* greedy (lexComment <* lexWhiteSpace)

lexToken :: Parser Char Token
lexToken = greedyChoice
             [ lexTerminal
             , lexEnum StdType stdTypes
             , lexEnum Operator operators
             , lexConstInt
             , lexConstBool
             , lexConstChar
             , lexLowerId
             , lexUpperId
             ]


lexTerminal :: Parser Char Token
lexTerminal = choice [t <$ keyword s | (t,s) <- terminals]
  where
    terminals :: [(Token, String)]
    terminals =
        [ ( POpen     , "("      )
        , ( PClose    , ")"      )
        , ( SOpen     , "["      )
        , ( SClose    , "]"      )
        , ( COpen     , "{"      )
        , ( CClose    , "}"      )
        , ( Comma     , ","      )
        , ( Semicolon , ";"      )
        , ( KeyIf     , "if"     )
        , ( KeyElse   , "else"   )
        , ( KeyWhile  , "while"  )
        , ( KeyFor    , "for"    )
        , ( KeyPrint  , "print"  )
        , ( KeyReturn , "return" )
        , ( KeyTry    , "try"    )
        , ( KeyCatch  , "catch"  )
        , ( KeyClass  , "class"  )
        , ( KeyVoid   , "void"   )        
        ]


lexEnum :: (String -> Token) -> [String] -> Parser Char Token
lexEnum f xs = f <$> choice (map keyword xs)

stdTypes :: [String]
stdTypes = ["int", "long", "double", "float", "byte", "short", "bool", "char"]
operators :: [String]
operators = ["+", "-", "*", "/", "%", "&&", "||", "^", "<=", "<", ">=", ">", "==", "!=", "="]


lexConstInt :: Parser Char Token
lexConstInt = ConstInt . read <$> greedy1 (satisfy isDigit)

lexConstBool :: Parser Char Token
lexConstBool = (ConstBool True <$ token "true") <|> (ConstBool False <$ token "false")

-- pack :: Parser s a -> Parser s b -> Parser s c -> Parser s
lexConstChar :: Parser Char Token
lexConstChar = ConstChar <$> pack (symbol '\'') anySymbol (symbol '\'')

lexComment :: Parser Char String
lexComment = token "//" *> greedy (satisfy (/= '\n')) <* satisfy (== '\n')

lexLowerId :: Parser Char Token
lexLowerId = (\x xs -> LowerId (x:xs)) <$> satisfy isLower <*> greedy (satisfy isAlphaNum)

lexUpperId :: Parser Char Token
lexUpperId = (\x xs -> UpperId (x:xs)) <$> satisfy isUpper <*> greedy (satisfy isAlphaNum)


lexWhiteSpace :: Parser Char String
lexWhiteSpace = greedy (satisfy isSpace)

keyword :: String -> Parser Char String
keyword [] = succeed ""
keyword xs@(x:_) | isLetter x = do ys <- greedy (satisfy isAlphaNum)
                                   guard (xs == ys)
                                   return ys
                 | otherwise  = token xs

greedyChoice :: [Parser s a] -> Parser s a
greedyChoice = foldr (<<|>) empty
----- End Lexer -----


----- Utilities for consuming tokens -----
sStdType :: Parser Token Token
sStdType = satisfy isStdType
    where isStdType (StdType _) = True
          isStdType _           = False

sUpperId :: Parser Token Token
sUpperId = satisfy isUpperId
    where isUpperId (UpperId _) = True
          isUpperId _           = False

sLowerId :: Parser Token Token
sLowerId = satisfy isLowerId
    where isLowerId (LowerId _) = True
          isLowerId _           = False

sConst :: Parser Token Token
sConst  = satisfy isConst
    where isConst (ConstInt  _) = True
          isConst (ConstBool _) = True
          isConst (ConstChar _) = True
          isConst _             = False

sOperator :: Parser Token Token
sOperator = satisfy isOperator
    where isOperator (Operator _) = True
          isOperator _            = False

sSemi :: Parser Token Token
sSemi =  symbol Semicolon

sComma :: Parser Token Token
sComma = symbol Comma
