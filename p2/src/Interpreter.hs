module Interpreter where

import ParseLib.Abstract hiding (empty)
import Prelude hiding ((<*), (<$), lookup)

import Data.Map hiding (map, foldr)
import qualified Data.Map as L

import Data.Char (isSpace)
import Control.Monad (replicateM)

import Data.List (elemIndex)
import Data.Maybe (fromMaybe)
import Lexer
import Parser
import Model
import Algebra



data Contents  =  Empty | Lambda | Debris | Asteroid | Boundary deriving (Eq, Show)

type Size      =  Int
type Pos       =  (Int, Int)
type Space     =  Map Pos Contents



-- | Parses a space file that can be found in the examples folder.
parseSpace :: Parser Char Space
parseSpace = do
    (mr, mc) <- parenthesised ((,) <$> natural <* symbol ',' <*> natural)
                <* spaces
    -- read |mr + 1| rows of |mc + 1| characters
    css      <- replicateM (mr + 1) (replicateM (mc + 1) contents)
    -- convert from a list of lists to a finite map representation
    return $ L.fromList $ concat $
            zipWith (\r cs ->
              zipWith (\c d -> ((r, c), d)) [0..] cs) [0..] css
  where
    spaces :: Parser Char String
    spaces = greedy (satisfy isSpace)

    contents :: Parser Char Contents
    contents = choice (Prelude.map (\(f,c) -> f <$ symbol c) contentsTable)
      <* spaces

-- | Conversion table
contentsTable :: [ (Contents, Char)]
contentsTable =  [ (Empty   , '.' )
                 , (Lambda  , '\\')
                 , (Debris  , '%' )
                 , (Asteroid, 'O' )
                 , (Boundary, '#' )]


--- DEBUG:START ---
testProgramString :: String
testProgramString = "start       -> turn right, go, turn left, firstArg.\n\nturnAround  -> turn right, turn right.\n\nreturn      -> case front of\nBoundary  ->  nothing;\n_         ->  go, return\nend.\n\nfirstArg    -> case left of\nLambda  ->  go, firstArg, mark, go;\n_       ->  turnAround, return, turn left, go, go, turn left,\nsecondArg\nend.\n\nsecondArg   -> case left of\nLambda  ->  go, secondArg, mark, go;\n_       ->  turnAround, return, turn left, go, turn left\nend."

testProgram :: Program
testProgram = [Rule "start" [CGo, CTurn DLeft, CCase DLeft [Alternative PAll [CGo, CTake]]], Rule "end" [CGo]]

testSpace :: Space
testSpace = fromList [((0,0), Boundary), ((0,1), Empty), ((0,2), Boundary), ((1,0), Empty), ((1,1), Empty), ((1,2), Empty)]

testSpace2 :: Space
testSpace2 = fst $ head $ parse parseSpace "(4,14)\n\\\\\\\\\\..........\n...............\\\\\\\\\\\\\\........\n...............\n..............."
--- DEBUG:END ---


-- Exercise 7
printSpace :: Space -> String
printSpace space = printContents list
  where list = assocs space

printContents :: [(Pos, Contents)] -> String
printContents [] = ""
printContents [(k,v)] = getCChar v : ""
printContents [(k1,v1),(k2,v2)]
  | fst k1 == fst k2 = getCChar v1 : getCChar v2 : ""
  | otherwise = getCChar v1 : '\n' : getCChar v2 : ""
printContents ((k1,v1):(k2,v2):xs)
  | fst k1 == fst k2 = getCChar v1 : printContents ((k2,v2):xs)
  | otherwise = getCChar v1 : '\n' : printContents ((k2,v2):xs)

-- Returns the corresponding Char of a Contents
getCChar :: Contents -> Char 
getCChar content = chars !! index
  where conts = map fst contentsTable
        chars = map snd contentsTable
        index = fromMaybe (-1) (elemIndex content conts)


-- These three should be defined by you
type Ident = String
type Commands = Model.Commands
data Heading = HLeft | HUp | HRight | HDown deriving (Enum, Show)

type Environment = Map Ident Interpreter.Commands

type Stack       =  Interpreter.Commands
data ArrowState  =  ArrowState Space Pos Heading Stack

data Step =  Done  Space Pos Heading
          |  Ok    ArrowState
          |  Fail  String


-- | Exercise 8
toEnvironment :: String -> Environment
toEnvironment string
  | check = foldr (\r rmap -> insert (ruleident r) (rulecmds r) rmap) empty rules
  | otherwise = error "Incorrect format!"
  where rules = calc $ alexScanTokens string
        check = checkProgram rules


-- | Exercise 9
step :: Environment -> ArrowState -> Step
step env (ArrowState space p h []) = Done space p h
step env state@(ArrowState space p h (c:cs)) = case c of
  CGo -> case lookup (nextP h p) space of
         (Just Empty)  -> Ok (ArrowState space (nextP h p) h cs)
         (Just Lambda) -> Ok (ArrowState space (nextP h p) h cs)
         (Just Debris) -> Ok (ArrowState space (nextP h p) h cs)
         _             -> sameState
  CTake -> case lookup p space of
           (Just Lambda) -> Ok (ArrowState putSpace p h cs)
           (Just Debris) -> Ok (ArrowState putSpace p h cs)
           _             -> sameState
  CMark -> Ok (ArrowState putMark p h cs)
  CNothing -> sameState
  (CTurn d) -> case h of
               HLeft ->  case d of
                         DLeft  -> Ok (ArrowState space p HDown cs)
                         DRight -> Ok (ArrowState space p HUp cs)
                         DFront -> Ok (ArrowState space p HLeft cs)
               HUp   ->  case d of
                         DLeft  -> Ok (ArrowState space p HLeft cs)
                         DRight -> Ok (ArrowState space p HRight cs)
                         DFront -> Ok (ArrowState space p HUp cs)
               HRight -> case d of
                         DLeft  -> Ok (ArrowState space p HUp cs)
                         DRight -> Ok (ArrowState space p HDown cs)
                         DFront -> Ok (ArrowState space p HRight cs)
               HDown  -> case d of
                         DLeft  -> Ok (ArrowState space p HRight cs)
                         DRight -> Ok (ArrowState space p HLeft cs)
                         DFront -> Ok (ArrowState space p HDown cs)
  (CCase d as)        -> case getCommands as (getCContents state d) of                         
                         Just cmds -> Ok (ArrowState space p h (cmds ++ cs))
                         Nothing -> Fail "No Alternative matches."
  (CIdentifier s)     -> case lookup s env of
                         Just cmds -> Ok (ArrowState space p h (cmds ++ cs))
                         Nothing   -> Fail "Rule not defined."
  where putSpace = insert p Empty space
        putMark = insert p Lambda space
        sameState = Ok (ArrowState putSpace p h cs)

-- Returns the commands of an Alternative when its Pattern matches with the Contents
getCommands :: [Alternative] -> Contents -> Maybe Interpreter.Commands
getCommands [] _ = Nothing
getCommands (a:as) contents
  | matches (altpattern a) contents = Just (altcmds a)
  | otherwise                       = getCommands as contents

-- Returns true whenever a Pattern matches with a certain Contents
matches :: Pattern -> Contents -> Bool 
matches PEmpty Empty       = True 
matches PLambda Lambda     = True 
matches PDebris Debris     = True 
matches PAsteroid Asteroid = True 
matches PBoundary Boundary = True 
matches PAll _             = True 
matches _ _                = False

-- Returns the next position of Arrow given a Heading
nextP :: Heading -> Pos -> Pos
nextP HLeft (x,y)  = (x + 1, y)
nextP HUp (x,y)    = (x, y - 1)
nextP HRight (x,y) = (x - 1, y)
nextP HDown (x,y)  = (x, y + 1)

-- Returns the corresponding Contents for a case command
getCContents :: ArrowState -> Direction -> Contents
getCContents (ArrowState space p HLeft _) DLeft   = fromMaybe Boundary $ lookup (nextP HDown p) space
getCContents (ArrowState space p HLeft _) DRight  = fromMaybe Boundary $ lookup (nextP HUp p) space
getCContents (ArrowState space p HLeft _) DFront  = fromMaybe Boundary $ lookup (nextP HLeft p) space
getCContents (ArrowState space p HUp _) DLeft     = fromMaybe Boundary $ lookup (nextP HLeft p) space
getCContents (ArrowState space p HUp _) DRight    = fromMaybe Boundary $ lookup (nextP HRight p) space
getCContents (ArrowState space p HUp _) DFront    = fromMaybe Boundary $ lookup (nextP HUp p) space
getCContents (ArrowState space p HRight _) DLeft  = fromMaybe Boundary $ lookup (nextP HUp p) space
getCContents (ArrowState space p HRight _) DRight = fromMaybe Boundary $ lookup (nextP HDown p) space
getCContents (ArrowState space p HRight _) DFront = fromMaybe Boundary $ lookup (nextP HRight p) space
getCContents (ArrowState space p HDown _) DLeft   = fromMaybe Boundary $ lookup (nextP HRight p) space
getCContents (ArrowState space p HDown _) DRight  = fromMaybe Boundary $ lookup (nextP HLeft p) space
getCContents (ArrowState space p HDown _) DFront  = fromMaybe Boundary $ lookup (nextP HDown p) space