module CSharpCode where

import Prelude hiding (LT, GT, EQ)
import qualified Data.Map as M

import CSharpLex
import CSharpGram
import CSharpAlgebra
import SSM
import Data.Char

{-
  This file contains a starting point for the code generation which should handle very simple programs.
-}




--                           clas memb stat  expr
codeAlgebra :: CSharpAlgebra Code Code Code (ValueOrAddress -> Code)
codeAlgebra =
    ( fClas
    , (fMembDecl, fMembMeth)
    , (fStatDecl, fStatExpr, fStatIf, fStatWhile, fStatFor, fStatPrint, fStatReturn, fStatBlock)
    , (fExprCon, fExprVar, fExprOp)
    )


fClas :: Env -> Token -> [Code] -> Code
fClas env c ms = [Bsr "main", HALT] ++ concat ms

fMembDecl :: Env -> Decl -> Code
fMembDecl env d = []

fMembMeth :: Env -> Type -> Token -> [Decl] -> Code -> Code
fMembMeth env t (LowerId x) ps s = [LABEL x] ++ s ++ [RET]

fStatDecl :: Env -> Decl -> Code
fStatDecl env d = []

fStatExpr :: Env -> (ValueOrAddress -> Code) -> Code
fStatExpr env e = e Value ++ [pop]

fStatIf :: Env -> (ValueOrAddress -> Code) -> Code -> Code -> Code
fStatIf env e s1 s2 = c ++ [BRF (n1 + 2)] ++ s1 ++ [BRA n2] ++ s2
    where
        c        = e Value
        (n1, n2) = (codeSize s1, codeSize s2)

fStatWhile :: Env -> (ValueOrAddress -> Code) -> Code -> Code
fStatWhile env e s1 = [BRA n] ++ s1 ++ c ++ [BRT (-(n + k + 2))]
    where
        c = e Value
        (n, k) = (codeSize s1, codeSize c)

-- Execute s1 once, while e == True, execute s2 and s3
fStatFor :: Env -> [Code] -> (ValueOrAddress -> Code) -> [Code] -> Code -> Code
fStatFor env sd1 e sd2 s = s1 ++ fStatWhile env e (s ++ s2)
  where s1 = concat sd1
        s2 = concat sd2

fStatPrint :: Env -> (ValueOrAddress -> Code) -> Code
fStatPrint env e = e Value ++ [TRAP 0]

fStatReturn :: Env -> (ValueOrAddress -> Code) -> Code
fStatReturn env e = e Value ++ [pop] ++ [RET]

fStatBlock :: Env -> [Code] -> Code
fStatBlock env = concat

fExprCon :: Env -> Token -> ValueOrAddress -> Code
fExprCon env (ConstInt n) va = [LDC n]
fExprCon env (ConstBool b) va | b         = [LDC 1]
                          | otherwise = [LDC 0]
fExprCon env (ConstChar c) va = [LDC (ord c)] 

fExprVar :: Env -> Token -> ValueOrAddress -> Code
fExprVar env (LowerId x) va = let loc = 37 in case va of
                                              Value    ->  [LDL  loc]
                                              Address  ->  [LDLA loc]

fExprOp :: Env -> Token -> (ValueOrAddress -> Code) -> (ValueOrAddress -> Code) -> ValueOrAddress -> Code
fExprOp env (Operator "=") e1 e2 va = e2 Value ++ [LDS 0] ++ e1 Address ++ [STA 0]
fExprOp env (Operator op) e1 e2 va 
  | op == "&&" = e1 Value ++ [LDS 0] ++ [BRF skipSize] ++ e2 Value ++ [AND]
  | op == "||" = e1 Value ++ [LDS 0] ++ [BRT skipSize] ++ e2 Value ++ [OR]
  | otherwise  = e1 Value ++ e2 Value ++ [opCodes M.! op]
  where skipSize = (codeSize (e2 Value) + 1) :: Int -- + 1, because of the [AND]/[OR]


opCodes :: M.Map String Instr
opCodes = M.fromList [ ("+", ADD), ("-", SUB),  ("*", MUL), ("/", DIV), ("%", MOD)
                   , ("<=", LE), (">=", GE),  ("<", LT),  (">", GT),  ("==", EQ)
                   , ("!=", NE), ("&&", AND), ("||", OR), ("^", XOR)
                   ]

-- | Whether we are interested in the value of a variable, or a pointer to it
data ValueOrAddress = Value | Address
    deriving Show
