-- cabal init --cabal-version=2.4 --license=NONE -p myfirstapp
-- cabal run :oefenen
-- cabal build :oefenen
-- ghci
-- :l main.hs
-- :r
-- :quit

module Main where

import Data.List
import Data.Char

--Exercise 1

unwords' :: [String] -> String
unwords' [] = ""
unwords' (x:[]) = x
unwords' (x:xs) = x ++ " " ++ unwords' xs

-- foldr :: Foldable t => (a -> b -> b) -> b -> t a -> b
unwords'' :: [String] -> String
unwords'' xs = foldr (\x y -> if y == [] then x else x ++ " " ++ y) "" xs

-- takeWhile :: (a -> Bool) -> [a] -> [a]
-- delete :: Eq a => a -> [a] -> [a]
-- dropWhile :: (a -> Bool) -> [a] -> [a]
words' :: String -> [String]
words' "" = []
words' x = (takeWhile (/= ' ') x) : (words' (delete ' ' (dropWhile (/= ' ') x)))

-- foldr :: Foldable t => (a -> b -> b) -> b -> t a -> b
-- foldr :: (Char -> (String -> [String]) -> (String -> [String])) (String -> [String]) (Char)
words'' :: String -> [String]
words'' xs = foldr f lijst xs $ ""
    where
        f :: Char -> (String -> [String]) -> (String -> [String])
        f c fx = \p -> let (s:ss) = fx [c]
            in case p of 
                "" -> dropWhile (== "") (s:ss)
                otherwise -> case (p == " ", s == "") of
                    (True, False) -> "":s:ss
                    (True, True) -> s:ss
                    otherwise -> (p++s):ss
        lijst :: String -> [String]
        lijst s = if s == " "
            then [""]
            else [s]

-- Exercise 2

-- foldr (+) e [a, b, c, d] == a + (b + (c + (d + e)))
-- bij foldr staan de hoop haakjes rechts
-- foldr :: (a -> b -> b) -> b -> [a] -> b
foldr'' :: (Int -> Int -> Int) -> Int -> [Int] -> Int 
foldr'' f a [] = a
foldr'' f a (x:xs) = f x (foldr'' f a xs)

-- foldl (+) e [a, b, c, d] == (((e + a) + b) + c) + d
-- bij foldl staan de hoop haakjes aan de linkerkant
-- foldl :: (a -> b -> a) -> a -> [b] -> a
foldl'' :: (Int -> Int -> Int) -> Int -> [Int] -> Int 
foldl'' f a [] = a
foldl'' f a (x:xs) = foldl'' f (a + x) xs

-- exercise 3
-- list of digits to one number
lodton :: [Int] -> Int
lodton [] = 0
lodton (x:xs) = foldl (\a b -> read ((show a) ++ (show b))) x xs

-- string of ints to int
soiti :: String -> Int
soiti [] = 0
-- assumption that string xs only contains of digits, exception not covered
soiti xs = read xs

-- exercise 4
data Tree a = Bin (Tree a) (Tree a) | Tip a

testboom12 :: Tree Int
testboom12 = Bin (Tip 3) (Bin (Tip 5) (Tip 8))

testboom21 :: Tree Int
testboom21 = Bin (Bin (Tip 1) (Tip 2)) (Bin (Tip 4) (testboom12))

information :: Tree a -> [a]
information (Tip x) = [x]
information (Bin x y) = information x ++ information y

-- Exercise 5
-- testboom12 geeft {3,58}
--   * 
--  / \             1
-- 3   *
--     /\           2
--    5  8
-- testboom21 geeft {12,4,3,58}
--      *                
--     / \           1   
--    *   *            
--   /\   /\         2
--  1  2 4  *        
--         / \       3   
--        3   * 
--           / \     4
--          5   8  
--
-- testboom11 geeft {3,5,59,6}
--      *
--     /\       1
--    *  6
--   /\         2
--  3  *
--     /\       3
--    5  *
--       /\     4
--      6  9 

testboom22 :: Tree Int
testboom22 = Bin (Bin (Tip 6) (Tip 4)) (Bin (Bin (Tip 7) (Tip 5)) (Tip 9))
-- testboom22 geeft {64,75,9}
--     *
--    /  \          1
--   *    *
--  /\   / \        2
-- 6 4  *   9
--     / \          3
--    7   5

testboomeind11 :: Tree Int
testboomeind11 = Bin (Tip 5) (Bin (Tip 6) (Bin (Tip 7) (Bin (Tip 9) (Tip 8))))
-- testboomeind11 geeft {5,6,7,98} is (Bin ())
--          *
--         / \          1
--        5   *
--           / \        2
--          6   *
--             / \      3
--            7   *
--               / \    4
--              9   8

pack :: Tree Int -> String
pack boom = "{" ++ structure boom ++ "}"
    where
        structure :: Tree Int -> String
        structure (Tip a) = show (lodton (information (Tip a)))
        structure (Bin (Tip x) (Tip y)) = (structure (Tip x)) ++ (structure (Tip y))
        structure (Bin x y) = (structure x) ++ "," ++ (structure y)

testpack :: String
testpack = pack testboom12

repack :: [String] -> String 
repack (x:[]) = x
repack (x:xs) = x ++ "," ++ repack xs

-- Exercise 6
unpack :: String -> Tree Int
unpack xs = if (length rest == 0) 
                then leafToTree left
                else (Bin (leafToTree left) (unpack restString))
    where 
        cleaned = (words''' (cleanCarbage xs))
        left = giveFirstPart cleaned
        rest = checkNoRest cleaned
        restString = "{" ++ repack rest ++ "}"

giveFirstPart :: [String] -> [String] 
giveFirstPart (x:xs) 
    | length x == 1 && length xs == 0 = [x]
    | length x == 1 = x : giveFirstPart xs
    | length x == 2 = [x]

checkNoRest :: [String] -> [String]
checkNoRest xs = if (lengteTwee)
    then giveTheRest xs
    else []
    where
        lengteTwee = length (dropWhile (\x -> length x < 2) xs) > 0

giveTheRest :: [String] -> [String]
giveTheRest (x:xs) 
    | length x == 2 = xs
    | otherwise = giveTheRest xs

leafToTree :: [String] -> Tree Int
leafToTree (x:xs) 
    | length x == 2 = (Bin (Tip (digitToInt (x !! 0))) (Tip (digitToInt (x !! 1))))
    | length x == 1 && length xs == 0 = (Tip (digitToInt (x !! 0)))
    | otherwise = (Bin (Tip (digitToInt (x !! 0))) (leafToTree xs))

cleanCarbage :: String -> String
cleanCarbage "" = ""
cleanCarbage (x:xs) 
    | x == '{' = cleanCarbage xs
    | x == '}' = cleanCarbage xs
    | otherwise = x : cleanCarbage xs

-- takeWhile :: (a -> Bool) -> [a] -> [a]
-- delete :: Eq a => a -> [a] -> [a]
-- dropWhile :: (a -> Bool) -> [a] -> [a]
words''' :: String -> [String]
words''' "" = []
words''' x = (takeWhile (/= ',') x) : (words''' (delete ',' (dropWhile (/= ',') x)))

-- testboom12 = {3,58}
--      *
--      /\
--     3  *
--        /\
--       5  8
-- pack (unpack (pack testboom12)) -> "{3,58}""

-- testboomeind11 = {5,6,7,98}
--          *
--         / \          1
--        5   *
--           / \        2
--          6   *
--             / \      3
--            7   *
--               / \    4
--              9   8
-- pack (unpack (pack testboomeind11)) -> "{5,6,7,98}"

-- testboom22 = {64,75,9}
--     *
--    /  \          1
--   *    *
--  /\   / \        2
-- 6 4  *   9
--     / \          3
--    7   5  
-- pack (unpack (pack testboom22)) -> "{64,75,9}"

testboombreed :: Tree Int
testboombreed = Bin (Bin (Bin (Bin (Tip 5) (Tip 6)) (Tip 3)) (Tip 1)) (Bin (Tip 2) (Bin (Tip 4) (Bin (Tip 7) (Tip 8))))
-- testboombreed = "{}"
--             *
--            / \
--           *   *
--          /\   /\
--         *  1 2  *
--        /\       /\ 
--       *  3     4  *
--      /\          / \
--     5  6        7   8
-- pack (unpack (pack testboombreed)) -> "{56,3,1,2,4,78}"

main :: IO ()
main = putStrLn "Hello, Haskell!"
