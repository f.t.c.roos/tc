-- Submission by Floris Roos 6240992 & Danny Eek 6590136

import ParseLib.Abstract
import System.Environment
import Prelude hiding ((<*), (*>))
import System.IO
import Data.Maybe
import Text.PrettyPrint.Boxes

-- Starting Framework

-- | "Target" datatype for the DateTime parser, i.e, the parser should produce elements of this type.
data DateTime = DateTime { date :: Date
                         , time :: Time
                         , utc :: Bool }
    deriving (Eq, Ord)

data Date = Date { year  :: Year
                 , month :: Month
                 , day   :: Day }
    deriving (Eq, Ord)

newtype Year  = Year { unYear :: Int }  deriving (Eq, Ord)
newtype Month = Month { unMonth :: Int } deriving (Eq, Ord)
newtype Day   = Day { unDay :: Int } deriving (Eq, Ord)

data Time = Time { hour   :: Hour
                 , minute :: Minute
                 , second :: Second }
    deriving (Eq, Ord)

newtype Hour   = Hour { unHour :: Int } deriving (Eq, Ord)
newtype Minute = Minute { unMinute :: Int } deriving (Eq, Ord)
newtype Second = Second { unSecond :: Int } deriving (Eq, Ord)


-- | The main interaction function. Used for IO, do not edit.
data Result = SyntaxError | Invalid DateTime | Valid DateTime deriving (Eq, Ord)

instance Show DateTime where
    show = printDateTime

instance Show Result where
    show SyntaxError = "date/time with wrong syntax"
    show (Invalid _) = "good syntax, but invalid date or time values"
    show (Valid x)   = "valid date: " ++ show x

main :: IO ()
main = mainDateTime

mainDateTime :: IO ()
mainDateTime = interact (printOutput . processCheck . processInput)
    where
        processInput = map (run parseDateTime) . lines
        processCheck = map (maybe SyntaxError (\x -> if checkDateTime x then Valid x else Invalid x))
        printOutput  = unlines . map show

mainCalendar :: IO ()
mainCalendar = do
    file:_ <- getArgs
    res <- readCalendar file
    putStrLn $ maybe "Calendar parsing error" (ppMonth (Year 2012) (Month 11)) res


-- Exercise 1
parseDateTime :: Parser Char DateTime
parseDateTime = DateTime <$> parseDate <*> parseTime <*> parseUtc

-- In the SAN format, we ignore the symbol T
parseDate :: Parser Char Date
parseDate = Date <$> parseYear <*> parseMonth <*> parseDay <* symbol 'T'
  where parseYear  = Year <$> parseFourDigits
        parseMonth = Month <$> parseTwoDigits
        parseDay   = Day <$> parseTwoDigits

parseTime :: Parser Char Time
parseTime = Time <$> parseHour <*> parseMinute <*> parseSecond
  where parseHour   = Hour <$> parseTwoDigits
        parseMinute = Minute <$> parseTwoDigits
        parseSecond = Second <$> parseTwoDigits

parseUtc :: Parser Char Bool
parseUtc = (=='Z') <$> (succeed 'e' <|> symbol 'Z') 

parseTwoDigits :: Parser Char Int -- (String -> [(Int, String)])
parseTwoDigits = (\x y -> 10*x + y) <$> newdigit <*> newdigit

parseFourDigits :: Parser Char Int
parseFourDigits = (\x y z a -> 1000*x + 100*y + 10*z + a) <$> newdigit <*> newdigit <*> newdigit <*> newdigit


-- Exercise 2
run :: Parser a b -> [a] -> Maybe b
run p s = case parse p s of [] -> Nothing
                            xs -> run' xs

-- e.g. (Int, String)
run' :: [(b, [a])] -> Maybe b
run' [] = Nothing
run' (x:xs)
  | null (snd x) = Just (fst x)
  | otherwise    = run' xs


-- Exercise 3
testDateTime :: DateTime
testDateTime = DateTime (Date (Year 1997) (Month 07) (Day 15)) (Time (Hour 03) (Minute 00) (Second 00)) True

-- Format: year-month-day-T-hour-minute-second-timeutc 
printDateTime :: DateTime -> String
printDateTime (DateTime (Date year month day) (Time hour minute second) utc) = show year' ++ showTwo month' ++ showTwo day' ++ "T" ++ showTwo hour' ++ showTwo minute' ++ showTwo second' ++ printUtc utc
  where year'   = unYear year
        month'  = unMonth month
        day'    = unDay day
        hour'   = unHour hour
        minute' = unMinute minute
        second' = unSecond second

printUtc :: Bool -> String
printUtc b
  | b         = "Z"
  | otherwise = ""

-- Makes sure that 2 digits are printed, even when holds: (month|day|hour|minute|second) < 10
showTwo :: Int -> String
showTwo x
  | x < 10    = "0" ++ show x
  | otherwise = show x


-- Exercise 4
parsePrint s = fmap printDateTime $ run parseDateTime s


-- Exercise 5
checkDateTime :: DateTime -> Bool
checkDateTime (DateTime (Date year month day) (Time hour minute second) _) = checkMonth month && checkDay year month day && checkHour hour && checkSixty minute' && checkSixty second'
  where minute' = unMinute minute
        second' = unSecond second

checkMonth :: Month -> Bool
checkMonth m = 1 <= m' && m' <= 12
  where m' = unMonth m

checkDay :: Year -> Month -> Day -> Bool
checkDay y m d
  | m' `elem` [1,3,5,7,8,10,12] = 1 <= d' && d' <= 31
  | m' `elem` [4,6,9,11]        = 1 <= d' && d' <= 30
  | otherwise                   = checkLeapYear y d
  where d' = unDay d
        m' = unMonth m

-- A year is a leap year if (it is divisible by 4 and not by 100) OR if (it is divisible by 400)
checkLeapYear :: Year -> Day -> Bool
checkLeapYear y d
  | (y' `mod` 4 == 0 && y' `mod` 100 /= 0) || (y' `mod` 400 == 0) = 1 <= d' && d' <= 29
  | otherwise                                                     = 1 <= d' && d' <= 28
  where y' = unYear y
        d' = unDay d

checkHour :: Hour -> Bool
checkHour h
  | 0 <= h' && h' <= 23 = True
  | otherwise           = False
  where h' = unHour h

-- Used for both minutes and seconds
checkSixty :: Int -> Bool
checkSixty x = 0 <= x && x <= 59 


-- Exercise 6
data Calendar = Calendar {prodid :: String, -- strictly 1
                          version :: String, -- strictly 1
                          events :: [Event]}
                          deriving (Eq, Ord)

instance Show Calendar where
  show c = printCalendar c


newtype Prodid = Prodid {unProdid :: String}
newtype Version = Version {unVersion :: String}


data Event = Event {dtStamp :: DateTime,
                    uid :: String, 
                    dtStart :: DateTime,
                    dtEnd :: DateTime,
                    description :: Maybe String, -- optionoal
                    summary :: Maybe String, -- optional
                    location :: Maybe String} -- optional
                    deriving (Eq, Ord)

instance Show Event where
  show e = printEvent e


data EventProp = DtStamp {unDtStamp :: DateTime} | Uid {unUid :: String} | DtStart {unDtStart :: DateTime} | DtEnd {unDtEnd :: DateTime} 
               | Description {unDescription :: String}| Summary {unSummary :: String} | Location {unLocation :: String}


-- Exercise 7
data Token = TEvent [Token]

           | TProdid String
           | TVersion String

           | TDtStamp DateTime
           | TDtStart DateTime
           | TDtEnd DateTime
           | TUid String
           | TDescription String
           | TSummary String
           | TLocation String
  deriving (Show, Eq, Ord)


-- scanCalendar :: String -> [([Token], String)]
scanCalendar :: Parser Char [Token]
scanCalendar = (++) <$> tcalendarHead <*> tcalendarEvents

-- Parser for the header of the calendar
tcalendarHead :: Parser Char [Token]
tcalendarHead = token "BEGIN:VCALENDAR" *> spaces *> crlf *> greedy (tversion <|> tprodid) <* spaces 

-- Parser for the list of events of the calendar
tcalendarEvents :: Parser Char [Token]
tcalendarEvents = greedy tevent <* spaces <* token "END:VCALENDAR" <* spaces <* crlf <* spaces <* eof

tevent :: Parser Char Token
tevent = TEvent <$> (token "BEGIN:VEVENT" *> spaces *> crlf *> spaces *> greedy teventprop <* spaces <* token "END:VEVENT" <* spaces <* crlf <* spaces)

teventprop :: Parser Char Token
teventprop = tdtstamp <|> tdtstart <|> tdtend <|> tuid <|> tdescription <|> tsummary <|> tlocation

-- DateTime Token Parsers
tdtstamp, tdtstart, tdtend :: Parser Char Token
tdtstamp = TDtStamp <$> (token "DTSTAMP:" *> spaces *> parseDateTime <* spaces <* crlf <* spaces)
tdtstart = TDtStart <$> (token "DTSTART:" *> spaces *> parseDateTime <* spaces <* crlf <* spaces)
tdtend = TDtEnd <$> (token "DTEND:" *> spaces *> parseDateTime <* spaces <* crlf <* spaces)

-- String Token Parsers
tuid, tdescription, tsummary, tlocation, tprodid, tversion :: Parser Char Token
tuid = TUid <$> (token "UID:" *> spaces *> lines' <* spaces <* crlf <* spaces)
tdescription = TDescription <$> (token "DESCRIPTION:" *> spaces *> lines' <* spaces <* crlf <* spaces)
tsummary = TSummary <$> (token "SUMMARY:" *> spaces *> lines' <* spaces <* crlf <* spaces)
tlocation = TLocation <$> (token "LOCATION:" *> spaces *> lines' <* spaces <* crlf <* spaces)

tprodid = TProdid <$> (token "PRODID:" *> spaces *> lines' <* spaces <* crlf <* spaces)
tversion = TVersion <$> (token "VERSION:" *> token "2.0" <* spaces <* crlf <* spaces)


-- Auxiliary functions
lines' :: Parser Char [Char]
lines' = (++) <$> line <*> line

line :: Parser Char [Char]
line = greedy (satisfy validChar) <* newlines

newlines :: Parser Char [Char]
newlines = concat <$> greedy (token "\r\n ")

spaces :: Parser Char String
spaces = greedy (satisfy isSpace)

isSpace :: Char -> Bool
isSpace x
  | x == ' '  = True
  | otherwise = False

validChar :: Char -> Bool
validChar x
  | x /= '\r'              = True
  | otherwise              = False

crlf :: Parser Char String
crlf = token "\r\n"


parseCalendar :: Parser Token Calendar
parseCalendar = Calendar <$> (parseProdid <|> parseVersion) <*> (parseProdid <|> parseVersion) <*> greedy parseEvent

parseEvent :: Parser Token Event
parseEvent = fromEvent <$> satisfy isEvent
fromEvent :: Token -> Event
fromEvent (TEvent tokens) = Event {dtStamp=dtstamp, uid=uid, dtStart=dtstart, dtEnd=dtend, description=description, summary=summary, location=location}
  where eventprops = map toEventProp tokens
        dtstamp = unDtStamp (head (filter (isProp "dtstamp") eventprops))
        uid = unUid (head (filter (isProp "uid") eventprops))
        dtstart = unDtStart (head (filter (isProp "dtstart") eventprops))
        dtend = unDtEnd (head (filter (isProp "dtend") eventprops))
        description = case filter (isProp "description") eventprops of
                      [] -> Nothing
                      xs -> Just (unDescription (head xs))
        summary = case filter (isProp "summary") eventprops of
                  [] -> Nothing
                  xs -> Just (unSummary (head xs))
        location = case filter (isProp "location") eventprops of
                   [] -> Nothing
                   xs -> Just (unLocation (head xs))                  
fromEvent _ = error "fromEvent"
isEvent :: Token -> Bool
isEvent (TEvent tokens) = isValidEvent tokens
isEvent _ = False

isProp :: String -> EventProp -> Bool
isProp "dtstamp" (DtStamp _) = True
isProp "uid" (Uid _) = True
isProp "dtstart" (DtStart _) = True
isProp "dtend" (DtEnd _) = True
isProp "description" (Description _) = True
isProp "summary" (Summary _) = True
isProp "location" (Location _) = True
isProp _ _ = False


isValidEvent :: [Token] -> Bool 
isValidEvent tokens = must isDtStamp && must isUid && must isDtStart && must isDtEnd && optional isDescription && optional isSummary && optional isLocation
  where must f = length (filter f tokens) == 1
        optional f = length (filter f tokens) == 1 || length (filter f tokens) == 0

toEventProp :: Token -> EventProp
toEventProp x
  | isDtStamp x = DtStamp (fromDtStamp x)
  | isUid x = Uid (fromUid x)
  | isDtStart x = DtStart (fromDtStart x)
  | isDtEnd x = DtEnd (fromDtEnd x)
  | isDescription x = Description (fromDescription x)
  | isSummary x = Summary (fromSummary x)
  | isLocation x = Location (fromLocation x)

parseProdid :: Parser Token String
parseProdid = fromProdid <$> satisfy isProdid
fromProdid :: Token -> String
fromProdid (TProdid x) = x
fromProdid _ = error "fromProdid"
isProdid :: Token -> Bool
isProdid (TProdid _) = True
isProdid _ = False

parseVersion :: Parser Token String
parseVersion = fromVersion <$> satisfy isVersion
fromVersion :: Token -> String
fromVersion (TVersion x) = x
fromVersion _ = error "fromVersion"
isVersion :: Token -> Bool
isVersion (TVersion _) = True
isVersion _ = False

fromDtStamp :: Token -> DateTime
fromDtStamp (TDtStamp x) = x
fromDtStamp _ = error "fromDtStamp"
isDtStamp :: Token -> Bool
isDtStamp (TDtStamp _) = True
isDtStamp _ = False

fromUid :: Token -> String
fromUid (TUid x) = x
fromUid _ = error "fromUid"
isUid :: Token -> Bool
isUid (TUid _) = True
isUid _ = False

fromDtStart :: Token -> DateTime
fromDtStart (TDtStart x) = x
fromDtStart _ = error "fromDtStart"
isDtStart :: Token -> Bool
isDtStart (TDtStart _) = True
isDtStart _ = False

fromDtEnd :: Token -> DateTime
fromDtEnd (TDtEnd x) = x
fromDtEnd _ = error "fromDtEnd"
isDtEnd :: Token -> Bool
isDtEnd (TDtEnd _) = True
isDtEnd _ = False

fromDescription :: Token -> String
fromDescription (TDescription x) = x
fromDescription _ = error "fromDescription"
isDescription :: Token -> Bool
isDescription (TDescription _) = True
isDescription _ = False

fromSummary :: Token -> String
fromSummary (TSummary x) = x
fromSummary _ = error "fromSummary"
isSummary :: Token -> Bool
isSummary (TSummary _) = True
isSummary _ = False

fromLocation :: Token -> String
fromLocation (TLocation x) = x
fromLocation _ = error "fromLocation"
isLocation :: Token -> Bool
isLocation (TLocation _) = True
isLocation _ = False

recognizeCalendar :: String -> Maybe Calendar
recognizeCalendar s = case mcalendar of Nothing -> mcalendar
                                        Just (Calendar p v events) -> if version calendar == "2.0" 
                                                                      then mcalendar
                                                                      else Just (Calendar v p events)
  where mcalendar = run scanCalendar s >>= run parseCalendar
        calendar = fromMaybe dummyCal mcalendar

dummyCal :: Calendar
dummyCal = Calendar{prodid="p", version="v", events=[]}


-- hGetContents :: Handle -> IO String
-- openFile FilePath -> IOMode -> IO Handle
-- hSetNewlineMode :: Handle -> NewlineMode -> IO ()

-- Exercise 8
readCalendar :: FilePath -> IO (Maybe Calendar)
readCalendar path = do handle <- openFile path ReadMode
                       _ <- hSetNewlineMode handle noNewlineTranslation
                       line <- hGetContents handle
                       return (recognizeCalendar line)


-- Exercise 9
-- DO NOT use a derived Show instance. Your printing style needs to be nicer than that :)
printCalendar :: Calendar -> String
printCalendar (Calendar prodid version events) = pbegin ++ pprodid ++ pversion ++ pevents ++ pend
  where pprodid = "PRODID:" ++ prodid ++ e
        pversion = "VERSION:" ++ version ++ e
        pevents = printEvents events
        pbegin = "BEGIN:VCALENDAR:" ++ e
        pend = "END:VCALENDAR" ++ e
        e = "\r\n"

printEvents :: [Event] -> String
printEvents xs = foldl (++) "" (map printEvent xs) 

printEvent :: Event -> String
printEvent (Event dtstamp uid dtstart dtend description summary location) = pbegin ++ pdtstamp ++ puid ++ pdtstart ++ pdtend ++ pdescription ++ psummary ++ plocation ++ pend
  where pdtstamp = "DTSTAMP:" ++ printDateTime dtstamp ++ e
        puid     = "UID:" ++ uid ++ e
        pdtstart = "DTSTART:" ++ printDateTime dtstart ++ e
        pdtend   = "DTEND:" ++ printDateTime dtend ++ e
        pdescription = printMaybeProp 1 description
        psummary = printMaybeProp 2 summary
        plocation = printMaybeProp 3 location
        pbegin = "BEGIN:VEVENT" ++ e
        pend = "END:VEVENT" ++ e
        e = "\r\n"

printMaybeProp :: Int -> Maybe String -> String
printMaybeProp 1 d = case d of Nothing -> ""
                               Just d  -> "DESCRIPTION:" ++ d ++ "\r\n"
printMaybeProp 2 s = case s of Nothing -> ""
                               Just s  -> "SUMMARY:" ++ s ++ "\r\n"
printMaybeProp 3 l = case l of Nothing -> ""
                               Just l  -> "LOCATION:" ++ l ++ "\r\n"
printMaybeProp _ _ = ""


-- Exercise 10
countEvents :: Calendar -> Int
countEvents (Calendar _ _ events) = length events

findEvents :: DateTime -> Calendar -> [Event]
findEvents datetime (Calendar _ _ events) = filter (\x -> dtStart x <= datetime && dtEnd x > datetime) events

checkOverlapping :: Calendar -> Bool
checkOverlapping calendar@(Calendar _ _ events) = any hasOverlap events
  where hasOverlap event = length (findEvents (dtStart event) calendar) >= 2

timeSpent :: String -> Calendar -> Int
timeSpent givenSummary calendar@(Calendar _ _ events) = foldl (\x y -> x + timeBetween y) 0 (filter (\x -> fromMaybe "" (summary x) == givenSummary) events)
  
timeBetween :: Event -> Int
timeBetween x = dateToMinutes (dtEnd x) - dateToMinutes (dtStart x)
    
dateToMinutes :: DateTime -> Int 
dateToMinutes dt@(DateTime (Date year month day) (Time hour minute _) _) = daysSinceYearZero (unYear year) * 24 * 60 + unMonth month * daysInThisMonth dt * 24 * 60 + unDay day * 24 * 60 + unHour hour * 60 + unMinute minute

daysSinceYearZero :: Int -> Int 
daysSinceYearZero 0 = 0
daysSinceYearZero x = daysInThisYear x + daysSinceYearZero (x - 1)

daysInThisYear :: Int -> Int
daysInThisYear x
  | isLeapYear x = 366
  | otherwise = 365

daysInThisMonth :: DateTime -> Int
daysInThisMonth (DateTime (Date year month _) _ _) 
  | unMonth month `elem` [1,3,5,7,8,10,12] = 31
  | unMonth month `elem` [4,6,9,11]        = 30
  | isLeapYear (unYear year) = 29
  | otherwise = 28

-- A year is a leap year if (it is divisible by 4 and not by 100) OR if (it is divisible by 400)
isLeapYear :: Int -> Bool
isLeapYear y = (y `mod` 4 == 0 && y `mod` 100 /= 0) || (y `mod` 400 == 0)


-- Due to time shortage, we did not manage to fully finish the last exercise
-- Exercise 11
ppMonth :: Year -> Month -> Calendar -> String
ppMonth year month (Calendar prodid version events) = render (emptyBox 1 1)
  where eventsThisMonth = filter (\x -> isThisMonth x year month) events

isThisMonth :: Event -> Year -> Month -> Bool
isThisMonth event y m
  | year d == y && month d == m = True
  | otherwise                   = False
  where d = date (dtStart event)


-- DEBUGGING:START -------------------------------------------------

-- Corresponds with bastille.ics
testCal1 :: Calendar
testCal1 = fromMaybe dummyCal (recognizeCalendar testCal1')

testCal1' :: String
testCal1' = "BEGIN:VCALENDAR\r\nVERSION:2.0\r\nPRODID:-//hacksw/handcal//NONSGML v1.0//EN\r\nBEGIN:VEVENT\r\nUID:19970610T172345Z-AF23B2@example.com\r\nDTSTAMP:19970610T172345Z\r\nDTSTART:19990714T170000Z\r\nDTEND:20010715T040000Z\r\nSUMMARY:Bastille Day Party\r\nEND:VEVENT\r\nEND:VCALENDAR\r\n"

testCal2' :: String
testCal2'  = "BEGIN:VCALENDAR\r\nPRODID:-//hacksw/handcal//NONSGMLv1.0//EN\r\nVERSION:2.0\r\nBEGIN:VEVENT\r\nUID:12345@example.com\r\nDTSTAMP:20111205T170000Z\r\nDTSTART:20111205T170000Z\r\nDTEND:20111205T210000Z\r\nSUMMARY:This is a very long description th\r\n at spans over multiple lines.\r\nEND:VEVENT\r\nEND:VCALENDAR\r\n"

-- Calendar with multiple events
testCal3' :: String
testCal3' = "BEGIN:VCALENDAR\r\nVERSION:2.0\r\nPRODID:-//hacksw/handcal//NONSGML v1.0//EN\r\nBEGIN:VEVENT\r\nUID:19970610T172345Z-AF23B2@example.com\r\nDTSTAMP:19970610T172345Z\r\nDTSTART:19970714T170000Z\r\nDTEND:19970715T040000Z\r\nSUMMARY:Bastille Day Party\r\nEND:VEVENT\r\nBEGIN:VEVENT\r\nUID:19970610T172345Z-AF23B2@example.com\r\nDTSTAMP:19970610T172345Z\r\nDTSTART:19970716T160000Z\r\nDTEND:19970719T180000Z\r\nDESCRIPTION:Booty Call\r\nEND:VEVENT\r\nBEGIN:VEVENT\r\nUID:19970610T172345Z-AF23B2@example.com\r\nDTSTAMP:19970610T172345Z\r\nDTSTART:19970719T170000Z\r\nDTEND:19970720T040000Z\r\nSUMMARY:Pool Party\r\nEND:VEVENT\r\nEND:VCALENDAR\r\n"

testCal3 :: Calendar
testCal3 = fromMaybe dummyCal (recognizeCalendar testCal3')

testCal4' :: String
testCal4' = "BEGIN:VCALENDAR\r\nVERSION:2.0\r\nPRODID:-//hacksw/handcal//NONSGML v1.0//EN\r\nBEGIN:VEVENT\r\nUID:19970610T172345Z-AF23B2@example.com\r\nDTSTAMP:19970610T172345Z\r\nDTSTART:19990714T170000Z\r\nDTEND:20010715T040000Z\r\nSUMMARY:Bastille Day Party\r\nEND:VEVENT\r\nEND:VCALENDAR\r\n"

testCal4 :: Calendar
testCal4 = fromMaybe dummyCal (recognizeCalendar testCal4')

readBastille, readMultiLine, readNewYear, readReadingClub :: IO (Maybe Calendar)
readBastille = readCalendar "C:/Users/6590136/Desktop/tc/p1/examples/bastille.ics"       -- Change this path for testing
readMultiLine = readCalendar "C:/Users/6590136/Desktop/tc/p1/examples/multiline.ics"     -- Change this path for testing
readNewYear = readCalendar "C:/Users/6590136/Desktop/tc/p1/examples/newyear.ics"         -- Change this path for testing
readReadingClub = readCalendar "C:/Users/6590136/Desktop/tc/p1/examples/readingclub.ics" -- Change this path for testing

readCalendar' :: FilePath -> IO String
readCalendar' path = do handle <- openFile path ReadMode
                        _ <- hSetNewlineMode handle noNewlineTranslation
                        hGetContents handle

-- DEBUGGING:END ---------------------------------------------------

