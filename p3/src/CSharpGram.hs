module CSharpGram where

import ParseLib.Abstract hiding (braced, bracketed, parenthesised)
import CSharpLex
import Prelude hiding ((<$), (<*), (*>))

data Class = Class Token [Member]
    deriving Show

data Member = MemberD Decl
            | MemberM Type Token [Decl] Stat
            deriving Show

data Stat = StatDecl   Decl
          | StatExpr   Expr
          | StatIf     Expr Stat Stat
          | StatWhile  Expr Stat
          | StatFor    [Stat] Expr [Stat] Stat
          | StatPrint  Expr
          | StatReturn Expr
          | StatBlock  [Stat]
          deriving Show

data Expr = ExprConst  Token
          | ExprVar    Token
          | ExprOper   Token Expr Expr
          deriving Show

data Decl = Decl Type Token
    deriving Show

data Type = TypeVoid
          | TypePrim  Token
          | TypeObj   Token
          | TypeArray Type
          deriving (Eq,Show)


pClass :: Parser Token Class
pClass = Class <$ symbol KeyClass <*> sUpperId <*> braced (many pMember)

pMember :: Parser Token Member
pMember =  MemberD <$> pDeclSemi
       <|> pMeth

pMeth :: Parser Token Member
pMeth = MemberM <$> methRetType <*> sLowerId <*> methArgList <*> pBlock
    where
        methRetType = pType <|> TypeVoid <$ symbol KeyVoid
        methArgList = parenthesised (option (listOf pDecl (symbol Comma)) [])

pBlock :: Parser Token Stat
pBlock = StatBlock <$> braced (many pStatDecl)

pStatDecl :: Parser Token Stat
pStatDecl =  pStat
         <|> StatDecl <$> pDeclSemi

-- greedy :: Parser s b -> Parser s [b]
-- greedy p = (:) <$> p <*> greedy p <<|> succeed []
pStatDeclFor :: Parser Token [Stat]
pStatDeclFor = (\x y -> x:y) <$> stat <*> greedy (sComma *> stat)
    where stat = (StatExpr <$> pExpr8Op) <|> (StatDecl <$> pDecl)

pStat :: Parser Token Stat
pStat =  StatExpr <$> pExpr8Op <*  sSemi
     <|> StatIf     <$ symbol KeyIf     <*> parenthesised pExpr8Op <*> pStat <*> optionalElse
     <|> StatWhile  <$ symbol KeyWhile  <*> parenthesised pExpr8Op <*> pStat
     -- for: "(" + statements + ";" + expression + ";" + statements + ")" + statements
     <|> StatFor    <$ symbol KeyFor    <*> (pOpen *> pStatDeclFor <* sSemi) <*> pExpr8Op <*> (sSemi *> pStatDeclFor <* pClose) <*> pStat
     <|> StatPrint  <$ symbol KeyPrint  <*> parenthesised pExpr8Op 
     <|> StatReturn <$ symbol KeyReturn <*> pExpr8Op               <*  sSemi
     <|> pBlock
     where optionalElse = option (symbol KeyElse *> pStat) (StatBlock [])
           pOpen  = symbol POpen
           pClose = symbol PClose
           expr   = StatExpr <$> pExpr8Op


pExprSimple :: Parser Token Expr
pExprSimple =  ExprConst <$> sConst
           <|> ExprVar   <$> sLowerId
           <|> parenthesised pExpr8Op

-- chainl, chainr :: Parser s a -> Parser s (a -> a -> a) -> Parser s a
pExpr1Op, pExpr2Op, pExpr3Op, pExpr4Op, pExpr5Op, pExpr6Op, pExpr7Op, pExpr8Op :: Parser Token Expr
pExpr8Op = chainr pExpr7Op    (exprOper priority1Op)
pExpr7Op = chainl pExpr6Op    (exprOper priority2Op)
pExpr6Op = chainl pExpr5Op    (exprOper priority3Op)
pExpr5Op = chainl pExpr4Op    (exprOper priority4Op)
pExpr4Op = chainl pExpr3Op    (exprOper priority5Op)
pExpr3Op = chainl pExpr2Op    (exprOper priority6Op)
pExpr2Op = chainl pExpr1Op    (exprOper priority7Op)
pExpr1Op = chainl pExprSimple (exprOper priority8Op)

-- Make tokens acceptable for chain functions
exprOper :: [Token] -> Parser Token (Expr -> Expr -> Expr)
exprOper [t] = ExprOper <$> symbol t
exprOper (t:ts) = (ExprOper <$> symbol t) <|> exprOper ts
exprOper _ = error "Error in exprOper"

-- Adjusted priorities, priority8 = highest priority
priority1Op, priority2Op, priority3Op, priority4Op, priority5Op, priority6Op, priority7Op, priority8Op :: [Token]
priority8Op = [Operator "*", Operator "/", Operator "%"]
priority7Op = [Operator "+", Operator "-"]
priority6Op = [Operator "<", Operator ">", Operator "<=", Operator ">="]
priority5Op = [Operator "==", Operator "!="]
priority4Op = [Operator "^"]
priority3Op = [Operator "||"]
priority2Op = [Operator "&&"]
priority1Op = [Operator "="]

pDecl :: Parser Token Decl
pDecl = Decl <$> pType <*> sLowerId

pDeclSemi :: Parser Token Decl
pDeclSemi = pDecl <* sSemi

pType :: Parser Token Type
pType = foldr (const TypeArray) <$> pType0 <*> many (bracketed (succeed ()))

pType0 :: Parser Token Type
pType0 =  TypePrim <$> sStdType
      <|> TypeObj  <$> sUpperId


-- The `Token` equivalents to some basic parser combinators
parenthesised, bracketed, braced :: Parser Token b -> Parser Token b
parenthesised p = pack (symbol POpen) p (symbol PClose) --(p)
bracketed     p = pack (symbol SOpen) p (symbol SClose) --[p]
braced        p = pack (symbol COpen) p (symbol CClose) --{p}
