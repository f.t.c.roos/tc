module CSharpAlgebra where

import CSharpLex
import CSharpGram
import qualified Data.Map

{-
  Only modify this file when you change the AST in CSharpGram.hs
-}
type Env = Data.Map.Map String Int -- (Variable name, address stored)
newMap = Data.Map.empty -- :: Map String Int

type CSharpAlgebra clas memb stat expr                --
    = (  Env -> Token -> [memb] -> clas                      --  Class = Class Token [Member]
                                                      --
      ,  ( Env -> Decl                             -> memb   --  Member = MemberD Decl
         , Env -> Type -> Token -> [Decl] -> stat  -> memb   --         | MemberM Type Token [Decl] Stat
         )                                            --
                                                      --
      ,  ( Env -> Decl                  -> stat              --  Stat = StatDecl   Decl
         , Env -> expr                  -> stat              --       | StatExpr   Expr
         , Env -> expr -> stat -> stat  -> stat              --       | StatIf     Expr Stat Stat
         , Env -> expr -> stat          -> stat              --       | StatWhile  Expr Stat
         , Env -> [stat] -> expr -> [stat] -> stat -> stat   --       | StatFor    [Stat] Expr [Stat] Stat
         , Env -> expr                  -> stat              --       | StatPrint  Expr
         , Env -> expr                  -> stat              --       | StatReturn Expr
         , Env -> [stat]                -> stat              --       | StatBlock  [Stat]
         )                                            --
                                                      --
      ,  ( Env -> Token                  -> expr             --  Expr = ExprConst  Token
         , Env -> Token                  -> expr             --       | ExprVar    Token
         , Env -> Token -> expr -> expr  -> expr             --       | ExprOper   Token Expr Expr
         )                                            --
      )


foldCSharp :: CSharpAlgebra clas memb stat expr -> Class -> clas
foldCSharp (c, (md,mm), (sd,se,si,sw,sf,sp,sr,sb), (ec,ev,eo)) = fClas newMap
    where
        fClas env (Class      t ms)       = c env t (map (fMemb env) ms)
        fMemb env (MemberD    d)          = md env d
        fMemb env (MemberM    t m ps s)   = mm env t m ps (fStat env s)
        fStat env (StatDecl   d)          = sd env d
        fStat env (StatExpr   e)          = se env (fExpr env e)
        fStat env (StatIf     e s1 s2)    = si env (fExpr env e) (fStat env s1) (fStat env s2)
        fStat env (StatWhile  e s1)       = sw env (fExpr env e) (fStat env s1)
        fStat env (StatFor    sd1 e sd2 s) = sf env (map (fStat env) sd1) (fExpr env e) (map (fStat env) sd2) (fStat env s)
        fStat env (StatPrint  e)          = sp env (fExpr env e)
        fStat env (StatReturn e)          = sr env (fExpr env e)
        fStat env (StatBlock  ss)         = sb env (map (fStat env) ss)
        fExpr env (ExprConst  con)        = ec env con
        fExpr env (ExprVar    var)        = ev env var
        fExpr env (ExprOper   op e1 e2)   = eo env op (fExpr env e1) (fExpr env e2)

