
# Open questions

## Exercise 4
It turns out that Happy is more efficient at parsing left-recursive structured rules.
According to the documentation they "result in a constant stack-space parser, whereas right-recursive rules require stack space proportional to the length of the list being parsed".
This means that left-recursion is actually preferred in Happy, which is in contrast to the preference of right-recursion in standard parser combinators.


## Exercise 10
A function call occupies memory on the stack with the details about the function. The memory occupied by the function is released after the function is finished.
A call for a recursive function will call itself, so for every call a memory block is created on the stack to hold the information about executing the recursive function.
When the recursion ends, it will return layer by layer on the stack to the outer function where the recursion started.
When the recursive call is somewhere in the middle of the stack, we need to add more stack size on the middle of the stack while no next commands of the sequence can be processed yet.
When such a recursive call happens at the end of the command sequence, the commands at the beginning of the sequence are already processed and removed from the stack when the call is handled.
The stack will then be less likely to overflow and its maximum size will be smaller.

In short, it matters whether the recursive call is in the middle of a command sequence or at the very end of the command sequence.