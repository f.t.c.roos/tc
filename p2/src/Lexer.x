-- https://www.haskell.org/alex/doc/alex.pdf
{
module Lexer where

import Model
}

%wrapper "basic"

$digit = 0-9      -- digits
$alpha = [a-zA-Z]   -- alphabetic characters
$ident = [$alpha $digit \+ \-] -- valid identifier

tokens :-
    $white+       ;
    "--".*       ;
    "->"         { \_ -> TArrow }
    "."          { \_ -> TPoint }
    ","          { \_ -> TComma }
    "go"         { \_ -> TGo }
    "take"       { \_ -> TTake }
    "mark"       { \_ -> TMark }
    "nothing"    { \_ -> TNothing }
    "turn"       { \_ -> TTurn }
    "case"       { \_ -> TCase }
    "of"         { \_ -> TOf }
    "end"        { \_ -> TEnd }
    "left"       { \_ -> TLeft }
    "right"      { \_ -> TRight }
    "front"      { \_ -> TFront }
    ";"          { \_ -> TSemicolon }
    "Empty"      { \_ -> TEmpty }
    "Lambda"     { \_ -> TLambda }
    "Debris"     { \_ -> TDebris }
    "Asteroid"   { \_ -> TAsteroid }
    "Boundary"   { \_ -> TBoundary }
    "_"          { \_ -> TAll }
    $ident+      { \s -> TIdentifier s }

{-- Each action has type :: String -> Token  

main = do
    s <- getContents
    print (alexScanTokens s)

}