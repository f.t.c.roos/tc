-- https://www.haskell.org/happy/doc/happy.pdf
{
module Parser where

import Model
}

%name calc
%tokentype { Token }
%error { parseError }

%token
    arrow      { TArrow }
    point      { TPoint }
    comma      { TComma }
    go         { TGo }
    take       { TTake }
    mark       { TMark }
    nothing    { TNothing }
    turn       { TTurn }
    case       { TCase }
    of         { TOf }
    end        { TEnd }
    left       { TLeft }
    right      { TRight }
    front      { TFront }
    semicolon  { TSemicolon }
    empty      { TEmpty }
    lambda     { TLambda }
    debris     { TDebris }
    asteroid   { TAsteroid }
    boundary   { TBoundary }
    underscore { TAll }
    ident      { TIdentifier $$ }

%% --Like yacc, we include%%here, for no real reason.

--Now we have the production rules for the grammar.

Program      : {- empty -}                        { [] }
             | Rule                               { [$1] }
             | Program Rule                       { $2 : $1 }

Rule         : ident arrow Commands point         { Rule $1 $3}
        
Command      : go                                 { CGo }
             | take                               { CTake }
             | mark                               { CMark }
             | nothing                            { CNothing }
             | turn Direction                     { CTurn $2 }
             | case Direction of Alternatives end { CCase $2 $4 }
             | ident                              { CIdentifier $1 }

Commands     : {- empty -}                        { [] }
             | Command                            { [$1] }
             | Commands comma Command             { $3 : $1 }

Direction    : left                               { DLeft }
             | right                              { DRight }
             | front                              { DFront }

Alternative  : Pattern arrow Commands             { Alternative $1 $3}

Alternatives : {- empty -}                        { [] }
             | Alternative                        { [$1] }
             | Alternatives semicolon Alternative { $3 : $1 }

Pattern      : empty                               { PEmpty }
             | lambda                              { PLambda }
             | debris                              { PDebris }  
             | asteroid                            { PAsteroid }
             | boundary                            { PBoundary } 
             | underscore                          { PAll }       

{
parseError :: [Token] -> a
parseError _ = error "Parse error"
}

