class Hello
{    
    void main()
    {        
        char a;
        a = 'a'; // exercise 1 (// // // // // // exercise 3)
        bool c;
        c = true; // exercise 1
        int z;
        z = 8 + 1 * 5; // exercise 2
        print (z) // exercise 8
        
        // if (3 && false) // <- this will handle an incorrect type exception when uncommented, exercise 12
        // {
        //     int z;  
        // }

        // a = 3 / 0; // <- this will handle a DivideByZeroException when uncommented, exercise 12

        for (int b, b = 0; b < 10; b = b + 1) // exercise 5
        {
            int c;
            print(b) // exercise 8
        }
    }    
}