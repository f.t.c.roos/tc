module Model where

-- Exercise 1
data Token = TArrow
           | TPoint
           | TComma
           | TGo 
           | TTake
           | TMark
           | TNothing
           | TTurn
           | TCase
           | TOf
           | TEnd
           | TLeft
           | TRight
           | TFront
           | TSemicolon
           | TEmpty
           | TLambda
           | TDebris
           | TAsteroid
           | TBoundary
           | TAll
           | TIdentifier String
           deriving (Eq, Show)


-- Exercise 2
type Program = [Rule]

data Rule = Rule {ruleident :: Identifier,
                  rulecmds :: Commands}
                  deriving Show
type Identifier = String

data Command = CGo
             | CTake
             | CMark
             | CNothing
             | CTurn Direction
             | CCase Direction Alternatives
             | CIdentifier Identifier
         
instance Show Command where
    show CGo = "-go-"
    show CTake = "-take-"
    show CMark = "-mark-"
    show (CTurn d) = "-turn:" ++ show d ++ "-"
    show (CCase d as) = "-case:" ++ show d ++ ":" ++ show as
    show (CIdentifier s) = "-ident:" ++ s
type Commands = [Command]

data Direction = DLeft | DRight | DFront deriving Show

data Alternative = Alternative {altpattern :: Pattern,
                                altcmds :: [Command]}
                                deriving Show
type Alternatives = [Alternative]

data Pattern = PEmpty | PLambda | PDebris | PAsteroid | PBoundary | PAll deriving (Eq, Show)



